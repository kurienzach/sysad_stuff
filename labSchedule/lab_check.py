#!/usr/bin/python3
import urllib.request
import re
from bs4 import BeautifulSoup
import json
import os.path, datetime, time

# Configuration variables
debug_mode = 1
nagios_cached_period = 1   # minutes
labs = {
    "nsl_nx": {
        "url": r'https://www.cse.iitb.ac.in/nagios/cgi-bin/status.cgi?hostgroup=99-nsl-annex-systems&style=overview',
        "json_file": "nsl_annexe_hosts.json"
    },
    "nsl": {
        "url": r'https://www.cse.iitb.ac.in/nagios/cgi-bin/status.cgi?hostgroup=99-nsl-systems&style=overview',
        "json_file": "nsl_systems_hosts.json"
    },
    "osl": {
        "url": r'https://www.cse.iitb.ac.in/nagios/cgi-bin/status.cgi?hostgroup=99-osl-systems&style=overview',
        "json_file": "osl_systems_hosts.json"
    }
}

def parse_nagios(html):
    """ 
    Parse the nagios page to populate down and critical list
    return - dict with 2 arrays down and critical
    """

    status = {
        "down": [],
        "critical": []
    }

    page = BeautifulSoup(html)

    # Filter out the table with class status
    status_table = page.find('table', class_="status")

    # Loop through each of the row of this table
    for row in status_table:
        if row.name != None:
            try:
                classname = row['class'][0]

                # System is down 
                if classname == "statusHOStdOWN":
                    status['down'] += [row.tr.td.a.string]
                else:
                    tds = row.find_all('td')

                    # System is critical
                    is_critical = tds[3].find('td', class_="miniStatusCRITICAL")
                    if is_critical != None:
                        status['critical'] += [row.tr.td.a.string]
            except:
                continue
    return status

def get_status_from_nagios_url(url):
    """
    Open nagios url and get the host down and critical status
    return - dict with 2 arrays down and critical
    """

    # set up authentication info
    auth_handler = urllib.request.HTTPBasicAuthHandler()
    auth_handler.add_password(realm="CSE LDAP ID",
                              uri=r'https://www.cse.iitb.ac.in/nagios/',
                              user='kurienzach',
                              passwd='pRistavo*453')
    opener =  urllib.request.build_opener(auth_handler)
    urllib.request.install_opener(opener)

    try:
        res = opener.open(url)
    except Exception as e:
        print (e.headers)
        print ( e.headers['www-authenticate'] )
        print("[ERROR] Error opening nagios page")
        exit()

    nodes = res.read()
    status = parse_nagios(nodes)
    status['down'].sort()
    status['critical'].sort()

    return status

def is_file_older(fp, minutes):
    if os.path.isfile(fp):
        mod_time = datetime.datetime.fromtimestamp(os.path.getmtime(fp))
        if (datetime.datetime.now() - mod_time).seconds < (minutes * 60):
            return False
    return True

def do_lab_check(lab=None):
    """
    Do lab check on the nagios page and do wifi + LAN check for NX systems 
    return - dicts with 2 arrays down and critical
    """

    nsl_annexe = {}
    nsl_systems = {} 
    osl_systems = {} 

    if lab == "nsl-nx" or lab == None:
        # Annexe Systems

        # Check if json files time stamps are older than nagios refresh
        # (possibily save some time!)
        if not is_file_older(labs['nsl_nx']['json_file'], nagios_cached_period):
            if debug_mode:
                print("Reading from cached NSL-NX hosts")
            with open(labs['nsl_nx']['json_file']) as fp:
                nsl_annexe = json.loads(fp.read())
        else:
            if debug_mode:
                print("Parsing NSL-NX hosts from nagios")
            nsl_annexe = get_status_from_nagios_url(labs['nsl_nx']['url'])

            # Loop to remove duplication with wifi
            temp_annexe_list = {
                "down": [],
                "critical": []
            }

            for system in nsl_annexe['critical']:
                sys_no = int(system.split('-')[1])
                if sys_no < 100:
                    temp_annexe_list['critical'].append(system)
                elif sys_no >= 100 and sys_no < 200:
                    if "nsl-"+str(sys_no+100) in nsl_annexe['critical']:
                        temp_annexe_list['critical'].append(system)
                    elif "nsl-"+str(sys_no+100) in nsl_annexe['down']:
                        temp_annexe_list['critical'].append(system)

            for system in nsl_annexe['down']:
                sys_no = int(system.split('-')[1])
                if sys_no < 100:
                    temp_annexe_list['down'].append(system)
                elif sys_no >= 100 and sys_no < 200:
                    if "nsl-"+str(sys_no+100) in nsl_annexe['down']:
                        temp_annexe_list['down'].append(system)
                    elif "nsl-"+str(sys_no+100) in nsl_annexe['critical']:
                        temp_annexe_list['critical'].append(system)

            nsl_annexe = temp_annexe_list
            with open(labs['nsl_nx']['json_file'], "w") as f:
                f.write(json.dumps(nsl_annexe))

    if lab == "nsl" or lab == None:
        # NSL Systems
        if not is_file_older(labs['nsl']['json_file'], nagios_cached_period):
            if debug_mode:
                print("Reading from cached NSL hosts")
            with open(labs['nsl']['json_file']) as fp:
                nsl_systems = json.loads(fp.read())
        else:
            if debug_mode:
                print("Parsing NSL hosts from nagios")
            nsl_systems = get_status_from_nagios_url(labs['nsl']['url'])
            with open(labs['nsl']['json_file'], "w") as f:
                f.write(json.dumps(nsl_systems))

    if lab == "osl" or lab == None:
        # OSL Systems
        if not is_file_older(labs['osl']['json_file'], nagios_cached_period):
            if debug_mode:
                print("Reading from cached OSL hosts")
            with open(labs['osl']['json_file']) as fp:
                osl_systems = json.loads(fp.read())
        else:
            if debug_mode:
                print("Parsing OSL hosts from nagios")
            osl_systems = get_status_from_nagios_url(labs['osl']['url'])
            with open(labs['osl']['json_file'], "w") as f:
                f.write(json.dumps(osl_systems))

    if debug_mode:
        print(nsl_annexe, nsl_systems, osl_systems)

    return nsl_annexe, nsl_systems, osl_systems

do_lab_check()

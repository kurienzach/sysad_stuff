#!/usr/bin/python3

import datetime
import pickle

groups = {
    "CS101_Group": {
        "current": 0,
        "updated": datetime.date(2014, 9, 25),
        "groups": [
            "Asif, Ankith, Kurien, Vivek",
            "Anubhav, Jigar, Paresh"
        ]
    },
    "UG_Lab_GroupA": {
        "current": 0,
        "updated": datetime.date(2014, 9, 25),
        "groups": [
            "Ankith",
            "Kurien",
            "Paresh",
            "Vivek"
        ]
    },
    "UG_Lab_GroupB": {
        "current": 0,
        "updated": datetime.date(2014, 9, 25),
        "groups": [
            "Asif",
            "Anubhav",
            "Jigar"
        ]
    }
}

schedule = {
    # Monday
    0: [
        ["10:00 AM", "12:00 PM", "Lab Check", "Fixed", "Anubhav, Jigar"],
        ["02:00 PM", "05:00 PM", "UG Lab", "RoundRobin", "UG_Lab_GroupA"],
        ["08:00 PM", "10:00 PM", "CS101", "RoundRobin", "CS101_Group"]
    ],
    # Tuesday
    1: [
        ["08:00 PM", "10:00 PM", "CS101", "RoundRobin", "CS101_Group"]
    ],
    # Wednesday
    2: [
        ["10:00 AM", "12:00 PM", "Lab Check", "Fixed", "Asif, Kurien"],
        ["08:00 PM", "10:00 PM", "CS101", "RoundRobin", "CS101_Group"]
    ],
    # Thursday
    3: [
        ["10:00 AM", "12:00 PM", "Lab Check", "Fixed", "Asif, Ankith"],
        ["02:00 PM", "05:00 PM", "UG Lab", "RoundRobin", "UG_Lab_GroupB"],
        ["08:00 PM", "10:00 PM", "CS101", "RoundRobin", "CS101_Group"]
    ],
    # Friday
    4: [
        ["08:00 PM", "10:00 PM", "CS101", "RoundRobin", "CS101_Group"]
    ],
    # Saturday
    5: [
        ["10:00 AM", "12:00 PM", "Lab Check", "Fixed", "Vivek, Paresh"],
    ]
}

pickle.dump(groups, open("groups.p", "wb"))
pickle.dump(schedule, open("schedule.p", "wb"))
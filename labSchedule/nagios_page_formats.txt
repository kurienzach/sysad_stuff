Host is critical
================
<tr class="statusEven">
    <td class="statusEven">
        <table border="0" width="100%" cellpadding="0" cellspacing="0">
            <tbody>
                <tr class="statusEven">
                    <td class="statusEven"><a href="status.cgi?host=osl-106&amp;style=detail" title="10.105.11.106">osl-106</a></td>
                </tr>
            </tbody>
        </table>
    </td>
    <td class="statusHOSTUP">UP</td>
    <td class="statusEven">
        <table border="0" width="100%">
            <tbody>
                <tr>
                    <td class="miniStatusOK">
                        <a href="status.cgi?host=osl-106&amp;servicestatustypes=2&amp;hoststatustypes=15&amp;serviceprops=0&amp;hostprops=0">6 OK</a>
                    </td>
                </tr>
                <tr>
                    <td class="miniStatusCRITICAL">
                        <a href="status.cgi?host=osl-106&amp;servicestatustypes=16&amp;hoststatustypes=15&amp;serviceprops=0&amp;hostprops=0">1 CRITICAL</a>
                    </td>
                </tr>
            </tbody>
        </table>
    </td>
    <td valign="center" class="statusEven">
        <a href="extinfo.cgi?type=1&amp;host=osl-106"><img src="/nagios/images/detail.gif" border="0" alt="View Extended Information For This Host" title="View Extended Information For This Host"></a>
        <a href="status.cgi?host=osl-106"><img src="/nagios/images/status2.gif" border="0" alt="View Service Details For This Host" title="View Service Details For This Host"></a>
        <a href="statusmap.cgi?host=osl-106"><img src="/nagios/images/status3.gif" border="0" width="20" height="20" alt="Locate Host On Map" title="Locate Host On Map"></a>
    </td>
</tr>

Host is down
============
<tr class="statusHOStdOWN">
    <td class="statusHOStdOWN">
        <table border="0" width="100%" cellpadding="0" cellspacing="0">
            <tbody>
                <tr class="statusHOStdOWN">
                    <td class="statusHOStdOWN"><a href="status.cgi?host=osl-24&amp;style=detail" title="10.105.11.24">osl-24</a></td>
                </tr>
            </tbody>
        </table>
    </td>
    <td class="statusHOStdOWN">DOWN</td>
    <td class="statusHOStdOWN">
        <table border="0" width="100%">
            <tbody>
                <tr>
                    <td class="miniStatusCRITICAL"><a href="status.cgi?host=osl-24&amp;servicestatustypes=16&amp;hoststatustypes=15&amp;serviceprops=0&amp;hostprops=0">7 CRITICAL</a></td>
                </tr>
            </tbody>
        </table>
    </td>
    <td valign="center" class="statusHOStdOWN">
        <a href="extinfo.cgi?type=1&amp;host=osl-24"><img src="/nagios/images/detail.gif" border="0" alt="View Extended Information For This Host" title="View Extended Information For This Host"></a>
        <a href="status.cgi?host=osl-24"><img src="/nagios/images/status2.gif" border="0" alt="View Service Details For This Host" title="View Service Details For This Host"></a>
        <a href="statusmap.cgi?host=osl-24"><img src="/nagios/images/status3.gif" border="0" width="20" height="20" alt="Locate Host On Map" title="Locate Host On Map"></a>
    </td>
</tr>

Host is up
===========
<tr class="statusEven">
    <td class="statusEven">
        <table border="0" width="100%" cellpadding="0" cellspacing="0">
            <tbody>
                <tr class="statusEven">
                    <td class="statusEven"><a href="status.cgi?host=osl-14&amp;style=detail" title="10.105.11.14">osl-14</a></td>
                </tr>
            </tbody>
        </table>
    </td>
    <td class="statusHOSTUP">UP</td>
    <td class="statusEven">
        <table border="0" width="100%">
            <tbody>
                <tr>
                    <td class="miniStatusOK"><a href="status.cgi?host=osl-14&amp;servicestatustypes=2&amp;hoststatustypes=15&amp;serviceprops=0&amp;hostprops=0">7 OK</a></td>
                </tr>
            </tbody>
        </table>
    </td>
    <td valign="center" class="statusEven">
        <a href="extinfo.cgi?type=1&amp;host=osl-14"><img src="/nagios/images/detail.gif" border="0" alt="View Extended Information For This Host" title="View Extended Information For This Host"></a>
        <a href="status.cgi?host=osl-14"><img src="/nagios/images/status2.gif" border="0" alt="View Service Details For This Host" title="View Service Details For This Host"></a>
        <a href="statusmap.cgi?host=osl-14"><img src="/nagios/images/status3.gif" border="0" width="20" height="20" alt="Locate Host On Map" title="Locate Host On Map"></a>
    </td>
</tr>
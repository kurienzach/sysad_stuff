#!/usr/bin/python3
import datetime
import pickle
import lab_check
import os
from optparse import OptionParser
import sys

usage = "usage: %prog [options]"

m_script_path="/home/kurien/Videos/labSchedule/"
m_yowsup_path="/home/kurien/Videos/"

def get_starting_hour(task):
    return datetime.datetime.strptime(task[0], "%I:%M %p").hour


def load_data(script_path):
    try:
        groups = pickle.load(open(script_path + "groups.p", "rb"))
        schedule = pickle.load(open(script_path + "schedule.p", "rb"))
    except:
        print("[ERROR]  Error opening pickle files")
        exit()

    return groups, schedule


def write_data(groups, schedule, script_path):
    pickle.dump(groups, open(script_path + "groups.p", "wb"))
    pickle.dump(schedule, open(script_path + "schedule.p", "wb"))


def append_labcheck_report():
    message = ""
    annexe_systems, nsl_systems, osl_systems = lab_check.do_lab_check()
    message += "Lab Check Report\n===================\n"
    message += "Annexe : " + str(len(annexe_systems)) + " down\n"
    message += ', '.join(annexe_systems) + "\n"
    message += "NSL : " + str(len(nsl_systems)) + " down\n"
    message += ', '.join(nsl_systems) + "\n"
    message += "OSL : " + str(len(osl_systems)) + " down\n"
    message += ', '.join(osl_systems) + "\n"
    return message


def send_watsapp_notif(message):
    # My number
    # ret = os.system('timeout 10s ' + m_yowsup_path \
    #     + 'yowsup/src/yowsup-cli -w -c '+ m_yowsup_path \
    #     + 'config.example -s 918095298035 "' + message + '"')

    # Temp group
    # ret = os.system('timeout 10s ' + m_yowsup_path \
    #     + 'yowsup/src/yowsup-cli -w -c ' + m_yowsup_path \
    #     + 'config.example -s 918095298035-1411284970 "' + message + '"')

    # Sysad group
    ret = os.system('timeout 10s ' + m_yowsup_path \
        + 'yowsup/src/yowsup-cli -w -c '+ m_yowsup_path \
        + 'config.example -s 918866289742-1406554009 "' + message + '"')

    if ret != 0:
        print("[ERROR] Watsapp send script returned " + str(ret))
        exit()


def get_tasklist_for_nextNhour(remind_before_hours):
    groups = dict()
    schedule = dict()

    # Load data saved in pickle
    groups, schedule = load_data(m_script_path)

    day_of_week = datetime.datetime.now().date().weekday()
    current_hour = datetime.datetime.now().time().hour

    # For testing
    # day_of_week = 3
    # current_hour = 18

    try:
        todays_task_list = schedule[day_of_week]
    except:
        print("[ERROR] Cant find any tasks for today")
        exit()

    if len(todays_task_list) == 0:
        print("[ERROR] Empty schedule")
        exit()

    if current_hour \
        < (get_starting_hour(todays_task_list[0]) - remind_before_hours):

        # Time is less than starting time of first task today
        print("Nothing to do. Too early. Exiting...")
        exit()
    elif current_hour \
        > (get_starting_hour(todays_task_list[-1]) - remind_before_hours):

        # Time is greater than starting time of last task today
        print("Nothing to do. Too late. Exiting...")
        exit()

    has_task = False

    message = "From @LazySysad\n\n"
    for task in todays_task_list:

        # Need to show only tasks in the next 'remind_before_hours' hours
        # Skip the list till we reach such a position
        if current_hour > get_starting_hour(task) - remind_before_hours:
            continue
        elif current_hour < get_starting_hour(task) - remind_before_hours:
            break

        has_task = True

        if task[3] == "Fixed":
            message += "@" + task[4] + "\n"
            message += task[0] + " - " + task[1] + " : " + task[2]
        elif task[3] == "RoundRobin":
            # Update RoundRobin current group value if not updated today
            if groups[task[4]]["updated"] != datetime.datetime.now().date():
                groups[task[4]]["current"] = (groups[task[4]]["current"] + 1) \
                                             % len(groups[task[4]]["groups"])
                groups[task[4]]["updated"] = datetime.datetime.now().date()

            current_group = groups[task[4]]["current"]
            message += "@" + groups[task[4]]["groups"][current_group] + "\n"
            message += task[0] + " - " + task[1] + " : " + task[2]

        message += "\n\n"

    # Write data to pickle
    # Writing is done at this point to ensure that even if failure happens
    # during labcheck or watsapp the RoundRobins are updated
    write_data(groups, schedule, m_script_path)

    if has_task:
        return message
    else:
        return None

if __name__ == '__main__':
    remind_before_hours = 2
    message = ""

    # Command Line Argument Parsin  g
    parser = OptionParser(usage = usage)
    parser.add_option("-w", "--no-watsapp", dest="enable_watsapp",
                      help="Avoid sending msg to watsap", 
                      action="store_false", default=True)
    (options, args) = parser.parse_args()

    print("\n###################################################################\n")
    print(datetime.datetime.now())

    message = get_tasklist_for_nextNhour(remind_before_hours)
    if message == None:
        print("No tasks for time being. Exiting...")
        exit()

    message += append_labcheck_report()

    print("==========================\n\t\tMessage\n==========================")
    print(message)

    if options.enable_watsapp:
        send_watsapp_notif(message)
    
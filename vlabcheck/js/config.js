// Node size and spacing
var node_size = 45;
var row_space = 25;
var node_space = 5;

// Initial drawing position
var x = 20;
var y = 20;

var nodes = [];
var node_count = 1;

// colors and text 
var up_color="#30FF6B";
var down_color="#FF5533";
var critical_color="#FF9433";
var update_txt_color="#8BA6B0";
var node_prefix="NSL-";
var topbar_height = 50;

var paper;
var refresh_interval = 90;
var refresh_cd = refresh_interval;
var refresh_timer;

var server_url = 'http://10.105.3.6:8080/'
//var server_url = 'http://localhost:8080/'


/* ################### Functions ################## */
function draw_node (x, y, id) {
   nodes[id] = paper.rect(x, y, node_size, node_size, node_size / 10);
   nodes[id].attr("fill", up_color);
   nodes[id].attr("stroke-width", 0);
   var t = paper.text(x + node_size/2, y + node_size/2, node_prefix + id);
}

function node_up (id) {
    nodes[id].animate({'fill': up_color}, 250, 'easein');
}

function node_down (id) {
    nodes[id].animate({'fill': down_color}, 250, 'easein');
}

function node_critical (id) {
    nodes[id].animate({'fill': critical_color}, 250, 'easein');
}

/**
 * Stretch the SVG to width or height depending on which is greater
 */
function stretch_svg () {
    var window_height = $(window).height() - topbar_height;
    var window_width = $(window).width();
    

    var svg_height = $('svg').height();
    var svg_width = $('svg').width();

    var ratio;

    (window_width / svg_width) > (window_height / svg_height) ? ratio = (window_height / svg_height) : ratio = (window_width / svg_width);

    $('svg').css("transform", "scale(" + ratio + ")");

    $('svg').css("left", $('body').width() / 2 - svg_width * ratio / 2);
    $('svg').css("top", 50);

    //alert(ratio + " " + window_height + " " + svg_height)
}

/**
 * Fire ajax and get the lab status from the REST server
 * @param {string} lab_name 
 * @param {ajax_url} ajax_url
 */
function get_status_from_server (lab_name, ajax_url) {
    // Fire AJAX request
    $.get(server_url + ajax_url, function(data) {
        console.log (data)
        var downList  = [];
        var criticalList  = [];

        // Make downList and criticalList from the JSON returned 
        data[lab_name]['down'].forEach(function (node) {
            var nodeId = parseInt(node.split('-')[1]);
            downList.push(nodeId);
        });
        data[lab_name]['critical'].forEach(function (node) {
            var nodeId = parseInt(node.split('-')[1]);
            criticalList.push(nodeId);
        });

        // Update title of the page 
        document.title = '(' + (downList.length + criticalList.length) + ')' + document.title.split(')')[1];
        
        // Redraw node
        redraw_nodes(downList, criticalList);

        // Start refreshing in n seconds timer
        var $refresh_txt = $('#refresh-text');
        $refresh_txt.text("Update in " + refresh_cd + "s");
        refresh_timer = setInterval(function(){
            $refresh_txt.text("Update in " + refresh_cd + "s");
            refresh_cd--;
            if (refresh_cd == 0) {
                $refresh_txt.text("Updating...");
                refresh_cd = refresh_interval;
                clearInterval(refresh_timer);
            }
        }, 1000);
    });

}

/**
 * Change color of nodes based on data from server
 * @param {number[]} downList 
 * @param {number[]} criticalList
 */
function redraw_nodes (downList, criticalList) {
    nodes.forEach(function(node, i) { 
        // If node is not already down and node present in downList bring node down
        if (node.attr('fill') != down_color && in_array(i, downList)) {
            node_down(i);
        }
        else if (node.attr('fill') != critical_color 
                 && in_array(i, criticalList)) {
            node_critical(i);  
        }
        else if (node.attr('fill') != up_color 
                 && !in_array(i, downList) 
                && !in_array(i, criticalList)) {
            node_up(i);  
        }
    });
}

/**
 * Helper function to check if element in array
 * @param {number} i
 * @param {number[]} array
 * @return {bool}
 */
function in_array (i, array) {
    var BreakException = {};

    try {
        array.forEach(function(item){
            if (item == i) {
                throw BreakException;
            }
        });
    }
    catch (e){
        if (e != BreakException) throw e;
        else return true;
    }
    return false;
}

/**
 * Bootrapping function for the App
 * @param {string} lab_name
 */
function start_app (lab_name) {
    // Set CSS to center the svg since applying these in CSS will be overridden when Raphael is drawn
    $('svg').css({
        'position': 'absolute',
        'display': 'block',
        'transform': 'scale(1)',
        'top': '50px',
        'left': '0'
    });

    // Stretch svg to oocupy full space
    window.onresize = stretch_svg;
    $(document).ready(function () {
        stretch_svg(); 
    })
    
    // Fire AJAX for lab status and schedule every refresh_interval seconds
    get_status_from_server(lab_name, lab_name);
    setInterval(get_status_from_server, refresh_interval * 1000, lab_name, lab_name);
}
